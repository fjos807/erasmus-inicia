# ERASMUS INICIA

Repositorio general para la replicación del contenido de la especialización en cualquier plataforma de enseñanza

## Pasos para la replicación

* [Demo](#demo)
* [Replicación del sitio](#replicación)
* [Ayuda](#ayuda)
 


## Demo

Puede encontrar una versión interactiva del sitio en: https://dazzling-ritchie-aa446f.netlify.app

## Replicación

Con el objetivo de replicar los contenidos de la especialización, se debe crear la misma estructura presente en este repositorio.

Se inicia copiando el código del archivo "index.html", que se encuentra en la raíz, y que mostrará la página de inicio. Para completar la navegación, también se debe copiar el contenido de todos los archivos presentes dentro de la carpeta "inicio".

Posteriormente, se copia el contenido de cada una de las carpetas de los módulos. Cada carpeta tiene un archivo de portada del módulo en cuestión y uno para cada asignatura.

Recuerde incluir el contenido de la carpeta "assets", misma que tiene todas las imágenes y documentos del sitio.

## Ayuda

Si necesita ayuda durante la replicación del contenido puede enviar un correo electrónico a fjos807@gmail.com
